
# Uncomment this and set it to your proxy address if needed
#http_proxy=http://proxy.latrobe.edu.au:8080

# Either galaxy-dist (stable) or galaxy-central
# 
galaxy_branch='galaxy-dist'

# Replace with your email address
# Use this email address when registering as a user in the galaxy interface
admin_email='iracooke@gmail.com'


if [[ -n $http_proxy ]]; then

	sudo touch /etc/apt/apt.conf
	sudo chmod o+w /etc/apt/apt.conf
	sudo echo "Acquire::http::Proxy \"$http_proxy\";" > /etc/apt/apt.conf
	sudo chmod o-w /etc/apt/apt.conf

	export http_proxy
	export https_proxy=$http_proxy

	sudo echo "export http_proxy=\"$http_proxy\";" > /home/vagrant/.bashrc
	sudo echo "export https_proxy=\"$http_proxy\";" >> /home/vagrant/.bashrc
fi

sudo apt-get update

# Galaxy base
sudo apt-get install -y build-essential autoconf automake git-core mercurial subversion pkg-config libc6-dev

# Extras to allow proteomics tools to work
sudo apt-get install -y libxml2-dev openjdk-6-jre

# To avoid needing to install the samtools toolshed package
sudo apt-get install -y samtools


mkdir /home/vagrant/bin
cd /home/vagrant/bin
wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/wigToBigWig
wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/bedGraphToBigWig
sudo chmod 755 wigToBigWig bedGraphToBigWig

wget https://github.com/arq5x/bedtools2/releases/download/v2.19.1/bedtools-2.19.1.tar.gz

tar -zxvf bedtools-2.19.1.tar.gz

cd bedtools2-2.19.1/
make

cd /home/vagrant/

sudo echo 'export PATH=$PATH:$HOME/bin' > /home/vagrant/.bashrc
sudo echo 'export PATH=$PATH:$HOME/bin/bedtools2-2.19.1/bin' > /home/vagrant/.bashrc


if [ ! -f /home/vagrant/virtualenv.py ]; then
	su vagrant -c "wget https://bitbucket.org/ianb/virtualenv/raw/tip/virtualenv.py"
fi

if [ ! -d /home/vagrant/galaxy_env ]; then
	su vagrant -c "python virtualenv.py --no-site-packages galaxy_env"
	su vagrant -c "echo '. ~/galaxy_env/bin/activate' >> ~/.bashrc"
fi

if [ ! -d /home/vagrant/$galaxy_branch ]; then
	if [ -d /vagrant/$galaxy_branch ]; then
		su vagrant -c "cp -r /vagrant/$galaxy_branch ./"
	else
		su vagrant -c "hg clone https://bitbucket.org/galaxy/$galaxy_branch"
		su vagrant -c "cd $galaxy_branch;hg update stable"
	fi
fi

if [ ! -f /home/vagrant/$galaxy_branch/universe_wsgi.ini ]; then
	cd /home/vagrant/$galaxy_branch
	su vagrant -c "cp universe_wsgi.ini.sample universe_wsgi.ini"
	sed -i.bak s/'#host = 127.0.0.1'/'host = 0.0.0.0'/ universe_wsgi.ini
	sed -i.bak s%'#tool_dependency_dir = None'%'tool_dependency_dir = ../tool_dependencies'% universe_wsgi.ini
	sed -i.bak s%'#tool_config_file = tool_conf.xml,shed_tool_conf.xml'%'tool_config_file = tool_conf.xml,shed_tool_conf.xml'% universe_wsgi.ini
	admin_users_string="admin_users = "$admin_email
	sed -i.bak "s%#admin_users = None%$admin_users_string%" universe_wsgi.ini
fi


