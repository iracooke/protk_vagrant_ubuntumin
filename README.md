## Setup

Setup a lightweight VM running Galaxy and with the [core galaxy dependencies installed](https://wiki.galaxyproject.org/Admin/Config/ToolDependenciesList)

1. Install [Virtualbox](https://www.virtualbox.org/) and [Vagrant](http://www.vagrantup.com/)

2. Clone this repository

		git clone git@bitbucket.org:iracooke/protk_vagrant_ubuntumin.git

3. Edit `bootstrap.sh`.  In particular, you should change `admin_email` to your email address

4. Start the virtual machine (this will take a while)

		cd protk_vagrant_ubuntumin
		vagrant up

5. Login to the newly created VM

		vagrant ssh

6. Start galaxy on the VM

		cd galaxy-dist
		sh run.sh --reload

7. Access Galaxy from your host machine by navigating to `localhost:8080`

8. Register as a new user with the email address provided in step 3